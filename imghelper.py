import re
import os
import io
import base64
from hashlib import sha1
import hmac

from PIL import Image
import boto3
from botocore.client import Config

import opts as Opts


endpoint_url = os.getenv("S3_MINIO_ENDPOINT") or None
# access key and secret pass via .env file
s3 = boto3.client('s3',
        endpoint_url=endpoint_url,
        aws_access_key_id=os.getenv("S3_ACCESS_KEY"),
        aws_secret_access_key=os.getenv("S3_SECRET_KEY"),
        config=Config(
            region_name=os.getenv("S3_REGION"),
            signature_version='s3v4')
        )


def parse_path(path):
    reg_pattern = r'/?(?P<hash>[^/]+)/(?P<hash_input>(?:opts(?P<opts>(?::[0-9a-zA-Z_]+\([^)]*\))*))/(?P<image>.+))'
    reg = re.compile(reg_pattern)
    result = reg.match(path)

    if not result:
        raise Exception("path is invalid")

    result = result.groupdict()

    img_path = result["image"]

    opts = result["opts"]
    opts = opts.split(":")
    opts = filter(lambda x: x != "", opts)

    hash_str = result["hash"]
    hash_input = result["hash_input"]

    def parse_fun(fun):
        # fun e.g. "fun(1,2,3)"
        splitted = fun.split("(")
        name = splitted[0]
        # [:-1] to remove the ending ")"
        args = splitted[1][:-1].split(",")
        parsed = {
            "name": name,
            "args": args
        }

        return parsed

    opts = map(parse_fun, opts)
    opts = list(opts)

    return img_path, opts, hash_str, hash_input


def assert_correct_hash(hash_str, hash_input):
    secret = os.getenv("THUMBOR_SECURITY_KEY")
    b_str_secret = secret.encode("utf-8")
    b_str_input = hash_input.encode("utf-8")
    correct_hash = hmac.new(b_str_secret, b_str_input, 'sha1').digest()
    b64_b_str_correct_hash = base64.urlsafe_b64encode(correct_hash)
    b64_str_correct_hash = b64_b_str_correct_hash.decode()
    if hash_str != b64_str_correct_hash:
            raise Exception("hash doesn't match")


def load_image(path):
    image = s3.get_object(
            Bucket=os.getenv("S3_BUCKET"),
            Key=path
        )

    return image['Body']


def compute_image(path, opts):
    image_fs = load_image(path)
    image = Image.open(image_fs)
    image_after_opts = apply_opts(image, opts)

    return image_after_opts


def apply_opts(image, opts):
    for opt in opts:
        # apply each opt here
        opt_name = opt["name"]
        opt_fun = getattr(Opts, opt_name, None)

        if opt_fun == None:
            raise Exception("opt doesn't exist")

        opt_args = opt["args"]
        result = opt_fun(image, opt_args)

        if result:
            image = result

    return image


def pillow_img_to_base64(image):
    buffer = io.BytesIO()
    format = image.format or "JPEG"

    if image.mode == "RGBA" and format == "JPEG":
        image = image.convert("RGB")

    image.save(buffer, format=format, quality=95, subsampling=0)
    base64_img = base64.b64encode(buffer.getvalue()).decode("utf-8")

    return base64_img
