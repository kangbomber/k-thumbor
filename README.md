# k-thumbor
on-the-fly image processing on aws lambda through rest-api inspired by thumbor

**supported operations**
- resize an image
- put a watermark on an image

# setup

first make sure you have serverless frame work installed

    $ git clone https://gitlab.com/kangbomber/k-thumbor

create .env file

    $ cd k-thumbor
    $ cp .env.template .env

additional steps for local development

    $ python3 -m venv k-thumbor-env
    $ source k-thumbor-env/bin/activate
    $ cd k-thumbor
    $ npm ci
    $ python3 -m pip install -r requirements.txt
    $ python3 -m pip install -r requirements.dev.txt

# run tests

    $ cd k-thumbor
    $ python3 -m pytest

# start server locally for local development
first make sure you have minio installed

    # create a folder "minio_data" for minio to keep the data
    # and save a file "200.jpg" in minio_data/my-bucket
    $ mkdir minio_data
    $ mkdir minio_data/my-bucket
    $ wget https://picsum.photos/200.jpg -P minio_data/my-bucket

    # start minio server
    $ minio server minio_data

start server locally with port 8888

    $ cd k-thumbor
    $ serverless offline --host 0.0.0.0 --httpPort 8888

try open a browser and type in
http://localhost:8888/unsafe/opts:thumbnail(100,100)/200.jpg
dont forget to edit .env file appropriately