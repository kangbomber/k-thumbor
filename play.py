import os
import sys
from timeit import default_timer as timer
from urllib.request import urlopen
import base64
import io

from dotenv import load_dotenv
load_dotenv()
from PIL import Image

import imghelper


#start = timer()
#
#for i in range(10):
#    image = imghelper.compute_image(
#        "just-T-shirt-pink.jpg",
#        [
#            {
#                "name": "thumbnail",
#                "args": [500, sys.maxsize]
#            },
#            {
#                "name": "watermark",
#                "args": [
#                    "artwork.png",
#                    0.240,
#                    0.30,
#                    0.35
#                ]
#            },
#        ]
#    )
#
#end = timer()
#print(end - start) # Time in seconds, e.g. 5.38091952400282

image = imghelper.compute_image(
    "just-T-shirt-pink.jpg",
    [
        {
            "name": "watermark",
            "args": [
                "artwork.png",
                "0.0",
                "0.0",
                "0.5"
            ]
        },
        {
            "name": "thumbnail",
            "args": ["2000", str(sys.maxsize)]
        },
    ]
)
#image.save('264-shirt.png')
#image.save('264-shirt-subsampling-0.png', subsampling=0)
#image.save('264-shirt-subsampling-0-q-100.png', subsampling=0, quality=100)
#image.save('264-shirt-subsampling-0-q-90.png', subsampling=0, quality=90)
#image.save('264-shirt-subsampling-0-q-95.png', subsampling=0, quality=95)
#image.save('264-shirt-q-95.png', quality=95)
#image.save('264-shirt-q-100.png', quality=100)
#image.save('264-shirt.jpg')
#image.save('264-shirt-subsampling-0.jpg', subsampling=0)
#image.save('264-shirt-subsampling-0-q-100.jpg', subsampling=0, quality=100)
#image.save('264-shirt-subsampling-0-q-90.jpg', subsampling=0, quality=90)
#image.save('264-shirt-subsampling-0-q-95.jpg', subsampling=0, quality=95)
#image.save('264-shirt-q-95.jpg', quality=95)
#image.save('264-shirt-q-100.jpg', quality=100)
#image.show()

#image = imghelper.compute_image('Untitled-1', [])
#image.get_format_mime_type()
print(image.size)
#base64_str = imghelper.pillow_img_to_base64(image)

#bytes_data = base64.b64decode(base64_str)
#fp = io.BytesIO(bytes_data)
#Image.open(fp).show()
