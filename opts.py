import sys

from PIL import Image

import imghelper


def example_opt(image, args):
    # 1st arg, image, is PILLOW's image instance
    # 2nd arg, args, is the arguments recieved from url
    # if you transform "image" directly then don't return anything
    # otherwise just return the new "image" result
    pass


def thumbnail(image, args):
    width = int(args[0])
    height = int(args[1])

    image.thumbnail(size=(width, height))


def watermark(image, args):
    path = args[0]
    percent_left = float(args[1])
    percent_top = float(args[2])
    percent_width = float(args[3])

    image_width, image_height = image.size

    watermark_fp = imghelper.load_image(path)
    watermark = Image.open(watermark_fp)

    wm_width, wm_height = watermark.size

    left = round(percent_left * image_width)
    top = round(percent_top * image_height)
    width = round(percent_width * image_width)
    height = round((width * wm_height) / wm_width)

    resized_wm = watermark.resize(size=(width, height))

    mask = None
    if resized_wm.mode == "RGBA":
        mask = resized_wm
    image.paste(resized_wm, box=(left, top), mask=mask)
