import imghelper
import os


def main(event, context):
    event_path = event["path"]
    img_path, opts, hash_str, hash_input = imghelper.parse_path(event_path)

    if os.getenv("THUMBOR_SECURITY_KEY"):
        imghelper.assert_correct_hash(hash_str, hash_input)

    computed_image = imghelper.compute_image(img_path, opts)
    base64_image = imghelper.pillow_img_to_base64(computed_image)
    mime_type = "image/" + computed_image.format.lower()

    return {
        "statusCode": 200,
        "body": base64_image,
        "isBase64Encoded": True,
        "headers": {
            "Content-Type": mime_type,
            "Access-Control-Allow-Origin": "*",
            "Cache-Control": "max-age=31536000, public"
        }
    }


if __name__ == "__main__":
    main('', '')
