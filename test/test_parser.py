from preggy import expect
from unittest.mock import patch, MagicMock
import os

from PIL import Image

import imghelper
import opts


def test_can_parse_path():
	# given
	path = '/hash/opts:fun():happy(1,,3)/path/to/img.jpg'

	# when
	image, opts, hash_str, hash_input = imghelper.parse_path(path)

	expected_image = "path/to/img.jpg"
	expected_opts = [
		{
			"name": "fun",
			"args": [""]
		},
		{
			"name": "happy",
			"args": [
				"1",
				"",
				"3"
			]
		}
	]

	#then
	expect(image).to_equal(expected_image)
	expect(opts).to_equal(expected_opts)
	expect(hash_str).to_equal('hash')
	expect(hash_input).to_equal('opts:fun():happy(1,,3)/path/to/img.jpg')


def test_can_assert_correct_hash_success():
        with patch.dict(os.environ, {"THUMBOR_SECURITY_KEY": "MY_SECURE_KEY"}):
                hash_input = "opts:watermark(artwork.png,0.3592,0.192,0.28159999999999996):thumbnail(400,400)/just-T-shirt-pink.jpg"
                hash_str = "314bJUd4yFHnEO4FII_6gsnc8-o="
                imghelper.assert_correct_hash(hash_str, hash_input)


def test_load_image_load_from_s3():
        # given
        bucket_name = "fake_bucket"
        fake_s3_object = {"Body": "inside_object_body"}

        fake_key = 'fake/s3/key'

        with patch.object(imghelper.s3,
                          'get_object',
                          return_value=fake_s3_object) as mock_get_object, \
                          patch.dict(os.environ, {"S3_BUCKET": bucket_name}):

                # when
                result = imghelper.load_image(fake_key)

                #then
                mock_get_object.assert_called_with(Bucket=bucket_name, Key=fake_key)


def test_can_compute_image():
        #imghelper.compute_image()
        image_path = "image_path"
        image_fp = "image_fp"
        pillow_image = "pillow_image"
        image_after_opts = "image_after_opts"
        opts = []

        with patch.object(imghelper, "load_image", return_value=image_fp) as load_image, \
             patch.object(Image, "open", return_value=pillow_image) as pillow_open, \
             patch.object(imghelper, "apply_opts", return_value=image_after_opts) as apply_opts:
                computed_img = imghelper.compute_image(image_path, opts)

                expect(computed_img).to_equal(image_after_opts)


        load_image.assert_called_with(image_path)
        pillow_open.assert_called_with(image_fp)
        apply_opts.assert_called_with(pillow_image, opts)


def test_opts_are_applied_to_image():
        # imghelper.apply_opts()
        fake_image = "fake_image"
        input_opts = [
                {
                        "name": "thumbnail",
                        "args": ["a", "b"]
                },
                {
                        "name": "watermark",
                        "args": ["c", "d"]
                }
        ]

        #opts.thumbnail(fake_image, [])
        # when
        with patch.object(opts, "thumbnail", return_value=None) as thumbnail, \
             patch.object(opts, "watermark", return_value=None) as watermark:
                imghelper.apply_opts(fake_image, input_opts)

        # then
        thumbnail.assert_called_with(fake_image, input_opts[0]["args"])
        watermark.assert_called_with(fake_image, input_opts[1]["args"])


def test_apply_opts_throw_nonexist_opt():
        pass


def test_can_turn_pillow_img_to_b64string():
        # given
        image = Image.open("fixtures/1x1-0000ff7f.png")

        # when
        base64_img = imghelper.pillow_img_to_base64(image)

        expected = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR4nGNgYPhfDwACggF/FjslngAAAABJRU5ErkJggg=='

        # then
        expect(base64_img).to_equal(expected)
